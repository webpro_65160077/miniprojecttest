type Material = {
    id: number
    name: string
    status: string
    min: number
    balance: number
    unit: string


  }

export { type Material }