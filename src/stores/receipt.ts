import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'

export const useReceiptStore = defineStore('receipt', () => {
    const authStore = useAuthStore()
    const memberStore = useMemberStore()
    const receiptDialog = ref(false)
    const receipt = ref<Receipt>({
        id: 0,
        createdDate: new Date(),
        total: 0,
        totalBefore: 0,
        memberDiscount: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: 'cash',
        userId: authStore.currentUser.id,
        user: authStore.currentUser,
        memberId: 0
    })
    const receiptItems = ref<ReceiptItem[]>([])
    const addReceiptItem = (product: Product) => {
        // the ? sign is called optional chaining to prevent error
        const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)

        if (index >= 0) {
            receiptItems.value[index].unit++
            calReceipt()
            return
        } else {
            const newReceipt: ReceiptItem = {
                id: -1,
                name: product.name,
                price: product.price,
                unit: 1,
                productId: product.id,
                product: product

            }

            receiptItems.value.push(newReceipt)
        }
        calReceipt()
    }
    const removeReceiptItem = (receiptItem: ReceiptItem) => {
        // the line below is called "callback function" to find index of array
        const index = receiptItems.value.findIndex((item) => item === receiptItem)
        receiptItems.value.splice(index, 1)
        calReceipt()
    }
    function inc(item: ReceiptItem) {
        item.unit++
    }
    const dec = (item: ReceiptItem) => {
        if (item.unit === 1) {
            removeReceiptItem(item)
            return
        }
        item.unit--
        calReceipt()
    }

    function calReceipt() {
        let totalBefore = 0
        for (const item of receiptItems.value) {
            totalBefore = totalBefore + (item.price * item.unit)
        }
        receipt.value.totalBefore = totalBefore
        // if current Member = null then discount 5%
        if (memberStore.currentMember) {
            receipt.value.total = totalBefore * 0.95
        } else {
            receipt.value.total = totalBefore
            console.log("this passed")
        }

    }
    function showReceiptDialog() {
        receiptDialog.value = true
        receipt.value.receiptItems = receiptItems.value
    }
    function clear() {
        receiptItems.value = []
        receipt.value = {
            id: 0,
            createdDate: new Date(),
            total: 0,
            totalBefore: 0,
            memberDiscount: 0,
            receivedAmount: 0,
            change: 0,
            paymentType: 'cash',
            userId: authStore.currentUser.id,
            user: authStore.currentUser,
            memberId: 0
        }
        memberStore.clear()
    }
    return {
        receiptItems,
        receipt, receiptDialog,
        addReceiptItem,
        removeReceiptItem,
        inc,
        dec, calReceipt, showReceiptDialog, clear
    }
})
